
package com.galvanize;

public class CallingCard {
    private int cost;
    private int minutes;

    public CallingCard(int cost){
        this.cost = cost;
    }

    public void addDollars(int dollars){
        int cents = dollars * 100;
        minutes += cents/cost;
    }

    public int getRemainingMinutes(){
        return Math.max(minutes, 0);
    }

    public void useMinutes(int minutesUsed){
        minutes -= minutesUsed;
    }
}
