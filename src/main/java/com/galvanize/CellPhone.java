
package com.galvanize;

import java.util.ArrayList;

public class CellPhone {
    private boolean isTalking = false;
    private ArrayList<String> callHistory = new ArrayList<>();
    private CallingCard card;
    private String currentCall;

    private int callMinutes;
    public CellPhone(CallingCard card){
        this.card = card;
    }

    public ArrayList<String> getHistory(){
        return callHistory;
    }

    public void setCurrentCall(String currentCall) {
        this.currentCall = currentCall;
    }

    public void setCallMinutes(int callMinutes){ this.callMinutes = callMinutes;}

    public void minutePassed(){
        callMinutes++;
    }

    public void call(String phoneNumber){
        isTalking = true;
        setCurrentCall(phoneNumber);
    }

    public void endCall(){
        isTalking = false;
        String historyMinutes = callMinutes != 1 ? String.format("%s minutes", callMinutes) : "1 minute";
        if (card.getRemainingMinutes() <= 0) {
            callHistory.add(String.format("%s (cut off at %s)", currentCall, historyMinutes));
        } else {
            callHistory.add(String.format("%s (%s)", currentCall, historyMinutes));
        }
        setCallMinutes(0);
    }

    public void tick(){
        if (isTalking){
            card.useMinutes(1);
            minutePassed();
            if (card.getRemainingMinutes() <= 0){
                endCall();
            }
        }

    }
}
